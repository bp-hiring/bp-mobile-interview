import 'package:bp_select/shared/generic_error_widget.dart';
import 'package:bp_select/shared/generic_loading_widget.dart';
import 'package:flutter/material.dart';

class CardImage extends StatelessWidget {
  final String imageUrl;
  final String placeholder;
  final VoidCallback? onTap;

  const CardImage({
    super.key,
    required this.imageUrl,
    required this.placeholder,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: AspectRatio(
        aspectRatio: 11 / 16,
        child: Card(
          clipBehavior: Clip.hardEdge,
          child: Image.network(
            imageUrl,
            fit: BoxFit.cover,
            errorBuilder: (_, __, ___) => GenericErrorWidget(
              message: placeholder,
              icon: Icons.image,
            ),
            loadingBuilder: (_, child, loader) =>
                loader != null ? const GenericLoadingWidget() : child,
          ),
        ),
      ),
    );
  }
}
