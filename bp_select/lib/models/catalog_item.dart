class CatalogItem {
  final String id;
  final String image;
  final String title;

  CatalogItem({
    required this.id,
    required this.image,
    required this.title,
  });

  factory CatalogItem.fromJson(Map<String, dynamic> json) {
    return switch (json) {
      {
        'id': String id,
        'image': String image,
        'title': String title,
      } =>
        CatalogItem(
          image: image,
          id: id,
          title: title,
        ),
      _ => throw const FormatException('Failed to parse item.'),
    };
  }
}
