class CatalogItemDetails {
  final String id;
  final String image;
  final String title;
  final String description;

  CatalogItemDetails({
    required this.id,
    required this.image,
    required this.title,
    required this.description,
  });

  factory CatalogItemDetails.fromJson(Map<String, dynamic> json) {
    return switch (json) {
      {
        'id': String id,
        'image': String image,
        'title': String title,
        'description': String description,
      } =>
        CatalogItemDetails(
          id: id,
          image: image,
          title: title,
          description: description,
        ),
      _ => throw const FormatException('Failed to parse item.'),
    };
  }
}
