import 'package:bp_select/models/catalog_item_details.dart';
import 'package:bp_select/details/details_screen_controller.dart';
import 'package:bp_select/details/details_screen_state.dart';
import 'package:bp_select/shared/card_image.dart';
import 'package:bp_select/shared/generic_error_widget.dart';
import 'package:bp_select/shared/generic_loading_widget.dart';
import 'package:flutter/material.dart';

class DetailsScreen extends StatefulWidget {
  final String id;
  const DetailsScreen({super.key, required this.id});

  @override
  State<DetailsScreen> createState() => _DetailsScreenState();
}

class _DetailsScreenState extends State<DetailsScreen> {
  final controller = DetailScreenController();

  @override
  void initState() {
    super.initState();
    controller.getById(widget.id);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: ValueListenableBuilder(
        valueListenable: controller,
        builder: (context, value, _) {
          return switch (value) {
            DetailsScreenLoadedState() => _buildLoaded(value),
            DetailsScreenErrorState() =>
              GenericErrorWidget(message: value.message),
            DetailsScreenLoadingState() => const GenericLoadingWidget(),
            DetailsScreenState() => const SizedBox.shrink(),
          };
        },
      ),
    );
  }

  Widget _buildLoaded(DetailsScreenLoadedState loadedState) {
    return SingleChildScrollView(
      child: Column(
        children: [
          _buildImage(loadedState.data),
          _buildTitle(loadedState.data.title),
          _buildSubtitle(loadedState.data.description),
        ],
      ),
    );
  }

  Widget _buildTitle(String title) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Text(
        title,
        style: Theme.of(context).textTheme.headlineSmall,
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget _buildSubtitle(String subtitle) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Text(
        subtitle,
        style: Theme.of(context).textTheme.bodyLarge,
      ),
    );
  }

  Widget _buildImage(CatalogItemDetails item) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 72.0, vertical: 32.0),
        child: CardImage(imageUrl: item.image, placeholder: item.title),
      ),
    );
  }
}
