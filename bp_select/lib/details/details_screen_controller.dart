import 'package:bp_select/details/details_screen_state.dart';
import 'package:bp_select/infra/catalog_repository.dart';
import 'package:flutter/material.dart';

class DetailScreenController extends ValueNotifier<DetailsScreenState> {
  final repository = CatalogRepository();

  DetailScreenController() : super(DetailsScreenLoadingState());

  void getById(String id) async {
    value = DetailsScreenLoadingState();
    try {
      final data = await repository.getDetails(id);
      value = DetailsScreenLoadedState(data: data);
    } catch (e) {
      value = DetailsScreenErrorState(e.toString());
    }
  }
}
