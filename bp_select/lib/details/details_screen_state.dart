import 'package:bp_select/models/catalog_item_details.dart';

abstract class DetailsScreenState {}

class DetailsScreenLoadingState extends DetailsScreenState {}

class DetailsScreenLoadedState extends DetailsScreenState {
  final CatalogItemDetails data;

  DetailsScreenLoadedState({required this.data});
}

class DetailsScreenErrorState extends DetailsScreenState {
  final String message;

  DetailsScreenErrorState(this.message);
}
