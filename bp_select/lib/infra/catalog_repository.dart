import 'dart:convert';
import 'dart:io';

import 'package:bp_select/models/catalog_item_details.dart';
import 'package:bp_select/models/catalog_item.dart';
import 'package:http/http.dart' as http;

class CatalogRepository {
  static String baseUrl =
      'https://us-central1-content-example.cloudfunctions.net';

  Future<CatalogItemDetails> getDetails(String id) async {
    try {
      final response = await http.get(Uri.parse('$baseUrl/catalog/$id'));

      if (response.statusCode == 200) {
        final json = jsonDecode(response.body) as Map<String, dynamic>;
        return CatalogItemDetails.fromJson(json);
      } else {
        throw HttpException(response.statusCode.toString());
      }
    } catch (e) {
      rethrow;
    }
  }

  Future<List<CatalogItem>> getAll() async {
    try {
      final response = await http.get(Uri.parse('$baseUrl/catalog'));

      if (response.statusCode == 200) {
        final json = jsonDecode(response.body) as Map<String, dynamic>;
        final items = json['items'];
        return items
            .map((jsonItem) {
              try {
                return CatalogItem.fromJson(jsonItem);
              } catch (_) {
                return null;
              }
            })
            .whereType<CatalogItem>()
            .toList();
      } else {
        throw HttpException(response.statusCode.toString());
      }
    } catch (e) {
      rethrow;
    }
  }
}
