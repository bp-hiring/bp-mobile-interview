import 'package:bp_select/home/home_screen_state.dart';
import 'package:bp_select/infra/catalog_repository.dart';
import 'package:flutter/material.dart';

class HomeScreenController extends ValueNotifier<HomeScreenState> {
  final repository = CatalogRepository();

  HomeScreenController() : super(HomeScreenLoadingState());

  void fetch() async {
    value = HomeScreenLoadingState();
    try {
      final data = await repository.getAll();
      if (data.isEmpty) {
        value = HomeScreenErrorState('Nenhum dado encontrado');
        return;
      }
      value = HomeScreenLoadedState(data: data);
    } catch (e) {
      value = HomeScreenErrorState(e.toString());
    }
  }
}
