import 'package:bp_select/details/details_screen.dart';
import 'package:bp_select/home/home_screen_controller.dart';
import 'package:bp_select/home/home_screen_state.dart';
import 'package:bp_select/models/catalog_item.dart';
import 'package:bp_select/shared/card_image.dart';
import 'package:bp_select/shared/generic_error_widget.dart';
import 'package:bp_select/shared/generic_loading_widget.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final controller = HomeScreenController();

  @override
  void initState() {
    super.initState();
    controller.fetch();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('BP Select'),
      ),
      body: ValueListenableBuilder(
        valueListenable: controller,
        builder: (context, value, _) {
          return switch (value) {
            HomeScreenLoadedState() => _buildLoaded(value),
            HomeScreenErrorState() =>
              GenericErrorWidget(message: value.message),
            HomeScreenLoadingState() => const GenericLoadingWidget(),
            HomeScreenState() => const SizedBox.shrink(),
          };
        },
      ),
    );
  }

  Widget _buildLoaded(HomeScreenLoadedState loadedState) {
    return GridView.count(
      crossAxisCount: 3,
      childAspectRatio: 11 / 16,
      padding: const EdgeInsets.all(8),
      children: loadedState.data.map(_buildItem).toList(),
    );
  }

  Widget _buildItem(CatalogItem item) {
    return CardImage(
      imageUrl: item.image,
      placeholder: item.title,
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
          builder: (_) => DetailsScreen(id: item.id),
        ));
      },
    );
  }
}
