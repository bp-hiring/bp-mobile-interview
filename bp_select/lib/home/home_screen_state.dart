import 'package:bp_select/models/catalog_item.dart';

abstract class HomeScreenState {}

class HomeScreenLoadingState extends HomeScreenState {}

class HomeScreenLoadedState extends HomeScreenState {
  final List<CatalogItem> data;

  HomeScreenLoadedState({required this.data});
}

class HomeScreenErrorState extends HomeScreenState {
  final String message;

  HomeScreenErrorState(this.message);
}
