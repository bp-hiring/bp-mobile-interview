# Catálago de Conteúdos Originais BP

Este é um aplicativo Flutter simples que apresenta uma lista de conteúdos originais da Brasil Paralelo e permite visualizar detalhes de cada conteúdo.

### Requisitos

- Flutter 3.16.3 ou superior
- Dispositivo Android ou iOS ou emulador/simulador para executar o aplicativo

### Como Executar

1. Certifique-se de ter o Flutter instalado. Para mais informações sobre como instalar o Flutter, consulte a [documentação oficial do Flutter](https://flutter.dev/docs/get-started/install).

2. Clone este repositório para o seu ambiente local:

    ```bash
    git clone https://gitlab.com/bp-hiring/bp-mobile-interview.git
    ```

3. Navegue até o diretório do projeto:
    ```bash
    cd mobile-interview/bp-select
    ```

4. Instale as dependências do projeto:
    ```bash
    flutter pub get
    ```

5. Execute o aplicativo em seu dispositivo ou emulador/simulador:
    ```bash
    flutter run
    ```

### Funcionalidades

O aplicativo possui as seguintes funcionalidades:

- **Home Screen**: Apresenta uma lista de conteúdos originais BP.
- **Details Screen**: Permite visualizar os detalhes de um conteúdo selecionado.

### Estrutura do Projeto

- `/home`: Widget, controller e arquivos relacionados à tela inicial.
- `/details`: Widget, controller e arquivos relacionados à tela de detalhes.
- `/infra`: Contém o repositório de catálogo de conteúdos.
- `/models`: Diretório contendo os modelos de dados utilizados no aplicativo.
- `/shared`: Diretório contendo widgets compartilhados e utilitários.